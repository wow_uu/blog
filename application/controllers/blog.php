<?php
	
	class Blog extends CI_Controller {
		
		function __construct(){
			parent::__construct();	
			$this->load->helper('url');
			$this->load->helper('form');
		}
		
		function index(){
			$data['title'] = 'My Blog Title';
			$data['heading'] = 'My Blog Head';
//			$this->db->where('is_public',1);
			$data['query'] = $this->db->get('entries');
			$this->load->view('blog_view',$data);
		}
		
		function comments(){
			$data['title'] = 'My Comment Title';
			$data['heading'] = 'My Comment Head';
			$this->db->where('entry_id',$this->uri->segment(3));
			$data['query'] = $this->db->get('comments');
			$this->load->view('comment_view',$data);
		}
		/**
		 * 发表评论
		 */
		function comment_insert(){
			$this->db->insert('comments',$_POST);
			redirect('blog/comments/'.$_POST['entry_id']);
		}
		/**
		 * 写信息
		 */
		function entries_insert(){
			if(empty($_POST)){
				$this->load->view('blog_add');
			}else{
				$userinfo = $this->session->userdata('userinfo');
				$data['author'] = $userinfo['username'];
				$data['body']	= $_POST['body'];
				$data['title']	= $_POST['title'];
				$data['is_public']	= $_POST['public'];
				$this->db->insert('entries',$data);
				redirect('blog');
			}
		}
	}
?>
