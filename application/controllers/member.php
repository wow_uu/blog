<?php
	
	class member extends CI_Controller {
		
		function __construct(){
			parent::__construct();	
			$this->load->helper('url');
			$this->load->helper('form');
		}
		
		function index(){
			$this->load->view('login');
		}
		
		function login(){
			$sql = "select * from user where username=? and password=? ";
			$query = $this->db->query($sql,array($_POST['username'],md5($_POST['password'])));
			
			if ($query->row()){
				$this->session->set_userdata('userinfo',$query->row_array());
				$this->load->view('blog_add');
			}else{
				$data['msg'] = 'Invalid username/email or password.';
				$this->load->view('login',$data);
			}
		}
		
		function register(){
			if(empty($_POST)){
				$this->load->view('register');
			}else{
				if (empty($_POST['username'])||empty($_POST['password'])){
					$data['msg'] = 'Invalid username or password.';
					$this->load->view('register',$data);
				}else{
					$param['username'] = $_POST['username'];
					$param['password'] = md5(trim($_POST['password']));
					$this->db->insert('user',$param);
					$this->session->set_userdata('userinfo',$param);
					redirect('blog');
				}
			}
		}
		
		function logout(){
			$this->session->sess_destroy();
			redirect('blog');
		}
		
	}
?>