﻿# MySQL-Front 5.0  (Build 1.33)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;


# Host: 127.0.0.1    Database: blog
# ------------------------------------------------------
# Server version 5.1.34-community

DROP DATABASE IF EXISTS `blog`;
CREATE DATABASE `blog` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `blog`;

#
# Table structure for table comments
#

CREATE TABLE `comments` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `entry_id` int(11) DEFAULT NULL,

  `body` text,

  `author` varchar(100) DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table comments
#
LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;

/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table entries
#

CREATE TABLE `entries` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `title` varchar(128) DEFAULT NULL,

  `body` text,

  `author` varchar(100) DEFAULT NULL,

  `is_public` tinyint(3) NOT NULL DEFAULT '1' COMMENT '0:no 1:public',

  PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='blog';

#
# Dumping data for table entries
#
LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;

/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table user
#

CREATE TABLE `user` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `username` varchar(100) NOT NULL DEFAULT '',

  `password` varchar(120) NOT NULL DEFAULT '',

  PRIMARY KEY (`id`),

  UNIQUE KEY `username` (`username`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table user
#
LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
